const { serve } = require('./src/api/app');

(async () => {
    const server = await serve('./src/courses.json', 8000);
    server.listen(server.settings.PORT, () =>
        console.log(`Now listening on port ${server.settings.PORT}`));
})();
