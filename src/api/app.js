const express = require('express');
const STATUS = require('http-status-codes');

const { read } = require('../file');

const app = express();
const router = express.Router();

let courses = [];

router.get('/courses/instructor/:name', ({ params: { name }}, res) => {
     const instructor_courses = courses.filter(({ terms }) => Object.values(terms)
            .map(term => term.instructors)
            .reduce((accumulator, value) => accumulator.concat(value), [])
            .map(instructor => instructor.toLowerCase())
            .some(instructor => instructor.includes(name.toLowerCase()))
    );

    res.status(STATUS.OK)
        .json(instructor_courses)
        .end();
});

module.exports.serve = async (file_path, port = 3000) => {
    const file_contents = await read(file_path, { encoding: 'utf16le' });
    const courses_object = JSON.parse(file_contents);

    courses = Object.entries(courses_object).map(([course_id, course]) => ({
        ...course,
        course_id: Number.parseInt(course_id, 10),
    }));

    app.use(router);
    app.set('PORT',  port);

    return app;
}
