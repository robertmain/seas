const fs = require('fs').promises;

module.exports.read = async (path, options) => {
    let file_contents = await fs.readFile(path, options);

    if (file_contents.charAt(0) === '\uFEFF') {
        file_contents = file_contents.substr(1);
    }

    return file_contents;
}
