const fs = require('fs').promises;

const request = require('supertest');
const STATUS = require('http-status-codes');

const app = require('api/app');

describe('Course catalog', () => {
    let api;

    beforeEach(async () => {
        jest.spyOn(fs, 'readFile')
            .mockImplementationOnce(async () => JSON.stringify({
                "101": {
                    "terms": {
                        "2015_spring": {
                            "instructors": [
                                "Leslie Valiant",
                                "Percy Adams"
                            ]
                        }
                    }
                },
                "102": {
                    "terms": {
                        "2014_fall": {
                            "instructors": [
                                "Leslie Valiant"
                            ]
                        }
                    }
                },
                "103": {
                    "terms": {
                        "2014_fall": {
                            "instructors": [
                                "Percy Adams"
                            ]
                        }
                    }
                },
            }));
        const server = await app.serve();
        api = request(server);
    });

    it('can be searched by full instructor name', done => {
        api.get('/courses/instructor/Leslie%20Valiant')
            .expect(STATUS.OK)
            .expect('Content-Type', /json/)
            .end((_err, response) => {
                const courses = response.body;

                expect(courses).toHaveLength(2);

                const [firstCourse, secondCourse] = courses;

                expect(firstCourse.course_id).toBeDefined();
                expect(firstCourse.course_id).toBe(101);
                expect(firstCourse.terms['2015_spring'].instructors).toHaveLength(2);
                expect(firstCourse.terms['2015_spring'].instructors).toContain('Leslie Valiant');

                expect(secondCourse.course_id).toBeDefined();
                expect(secondCourse.course_id).toBe(102);
                expect(secondCourse.terms['2014_fall'].instructors).toHaveLength(1);
                expect(secondCourse.terms['2014_fall'].instructors).toContain('Leslie Valiant');

                done();
            });
    });

    it('can be searched by partial match of instructor name', done => {
        api.get('/courses/instructor/Perc')
            .expect(STATUS.OK)
            .expect('Content-Type', /json/)
            .end((_err, response) => {
                const courses = response.body;

                expect(courses).toHaveLength(2);

                const [firstCourse, secondCourse] = courses;

                expect(firstCourse.course_id).toBeDefined();
                expect(firstCourse.course_id).toBe(101);
                expect(firstCourse.terms['2015_spring'].instructors).toHaveLength(2);
                expect(firstCourse.terms['2015_spring'].instructors).toContain('Percy Adams');

                expect(secondCourse.course_id).toBeDefined();
                expect(secondCourse.course_id).toBe(103);
                expect(secondCourse.terms['2014_fall'].instructors).toHaveLength(1);
                expect(secondCourse.terms['2014_fall'].instructors).toContain('Percy Adams');

                done();
            });
    });

    it('can be searched without case sensitivity', done => {
        api.get('/courses/instructor/pERc')
            .expect(STATUS.OK)
            .expect('Content-Type', /json/)
            .end((_err, response) => {
                const courses = response.body;

                expect(courses).toHaveLength(2);

                const [firstCourse, secondCourse] = courses;

                expect(firstCourse).toBeDefined();
                expect(firstCourse.terms['2015_spring'].instructors).toHaveLength(2);
                expect(firstCourse.terms['2015_spring'].instructors).toContain('Percy Adams');

                expect(secondCourse).toBeDefined();
                expect(secondCourse.terms['2014_fall'].instructors).toHaveLength(1);
                expect(secondCourse.terms['2014_fall'].instructors).toContain('Percy Adams');

                done();
            });
    })
});
