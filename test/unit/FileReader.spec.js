const fs = require('fs').promises;
const { read } = require('file');

describe('File Reader', () => {
    it('strips byte order mark from files', async () => {
        jest.spyOn(fs, 'readFile')
            .mockImplementationOnce(async () => '\uFEFFhello world');

        const file_contents = await read('./src/myfile.txt');

        expect(file_contents).not.toContain('\uFEFF');
        expect(file_contents).toBe('hello world');
    });

    it('parses files without byte order mark', async () => {
        jest.spyOn(fs, 'readFile')
            .mockImplementationOnce(async () => 'hello world');

        const file_contents = await read('./src/myfile.txt');

        expect(file_contents).toBe('hello world');
    });

    it('bubbles file read errors', () => {
        jest.spyOn(fs, 'readFile')
            .mockImplementationOnce(async () => {
                throw new Error('Unable to read file');
            });

        return expect(read('./nofile.txt'))
            .rejects.toThrowError('Unable to read file');
    });
});
